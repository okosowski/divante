$(function() {
    var menu = $('nav ul');
    var menuIsOnView = false;

    function toggleMenu(){
        if(menuIsOnView){
            $(menu).animate({'right': '-200px'});
        }else{
            $(menu).animate({'right': '0'});
        }
        menuIsOnView = !menuIsOnView;
    }

    $('html').on('click', function(e) {
        e.preventDefault();
        if(menuIsOnView){
            toggleMenu();
        }else{
            if(e.target.id == 'navButton'){
                toggleMenu();
            }
        }
    });

    $('.owl-carousel').owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        navigation:false,
        loop:true,
        margin:0,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav: false

            },
            640:{
                items:2,
                nav: false
            },
            980:{
                items:3,
                nav: false
            }
        }
    })

});
